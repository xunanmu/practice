/*
	Author: 徐南木 
	Date: 11/09/19 19:47
	Description: 编写一个程序来读取。将这些数字相加，并将结果赋给一个整数。最后，显示所有三个变量的值
*/
#include <stdio.h>
int main()
{
	int sum=0;
	float f1,f2;
	printf("请输入两个浮点数(并用空格隔开):");
	scanf("%f%f",&f1,&f2);
	sum=f1+f2;
	printf("%.3f+%.3f=%d",f1,f2,sum);
	return 0;	
} 

