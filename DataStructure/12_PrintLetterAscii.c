/*
	Author: 徐南木 
	Date: 11/09/19 19:49
	Description:编写一个程序来读取和打印一个字符。还要打印它的ASCII值。如果字符是小写的，则用大写打印，反之亦然。重复这个过程，直到输入一个*。 
*/
#include <stdio.h>
int main()
{
	char c;
	printf("请输入字母(以*结束)\n");
	while((c=getchar())!='*')
	{
		if(c>='a'&&c<='z')
			c=c-32;
		printf(" %c,ASCII=%d\n",c,c);
	}
	return 0;
}

