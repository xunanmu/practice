#include <stdio.h>
#include <math.h>

int fibonacci_1(int n);//递归解法 
int fibonacci_2(int n);//通项公式法

int main()
{
	int n;
	printf("请输入斐波那契项数(0<n<=46)\nn=");
	scanf("%d",&n);
	printf("斐波那契第%d项为%d",n,fibonacci_1(n));
	return 0; 
 }
  
int fibonacci_1(int n)
{
	return n<2? n: (fibonacci_1(n-1)+fibonacci_1(n-2));
}

int fibonacci_2(int n)
{
	return ( pow((1+sqrt(5))/2 ,n) - pow((1-sqrt(5))/2 ,n) )/sqrt(5);
}

