#include <stdio.h>
#include <stdlib.h> 
void rhombus(int n)
{
	int x,y;
	for(y=0;y<=2*n;++y,puts(""))
	{
		for(x=0;x<=2*n;++x)
			putchar(y<=n?abs(x-n)<=abs(y)?'*':' ':abs(x-n)<=abs(y-2*n)?'*':' ');
	}
}
int main(void)
{
	rhombus(3);
	return 0;
}

