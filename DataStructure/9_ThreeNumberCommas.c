/*
	Name: ThreeNumberCommas
	Copyright: public
	Author: 徐南木 
	Date: 11/09/19 18:52
	Description:编写一个程序来读取10个整数。通过在以逗号分隔的行中打印三个数字来显示这些数字。 
*/
#include <stdio.h>
void threeNumberCommas(int );

int main()
{
	printf("请输入10个整数(并用空格隔开)\n"); 
	threeNumberCommas(10);
	return 0;
}
void threeNumberCommas(int n)
{
	int num=0,i=0;
	while(++i<=n)
	{
		scanf("%d",&num);
		printf("%3d",num);
		i%3&&i!=n?putchar(','):puts("");
	}
}
