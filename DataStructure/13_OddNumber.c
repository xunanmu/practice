/*
	Author: 徐南木 
	Date: 11/09/19 19:49
	Description: 编写一个程序来打印从m到n的所有奇数。
*/
#include <stdio.h>
int main()
{
	int m,n;
	printf("请输入m,n两个整数(空格隔开,m<n):");
	scanf("%d%d",&m,&n);
	printf("%d到%d的奇数有:\n",m,n);
	do{
		if(m&1)
			printf("%d ",m);
	}while(++m<=n);
	return 0;
}
