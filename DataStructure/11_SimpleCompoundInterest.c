
/*
	Author: 徐南木 
	Date: 11/09/19 19:49
	Description: 编写计算程序。
*/
#include <stdio.h>
#include <math.h>

float  simpleInterest(int ,int );
float compoundInterest(int ,int  );

int main()
{
	int money,year;
	printf("请输入您的本金:");
	scanf("%d",&money);
	printf("请输入您的年限:");
	scanf("%d",&year);
	printf("单利:%.3f  复利:%.3f",simpleInterest(money,year),compoundInterest(money,year)); 
	return 0;
}
/*单利计算：仅用本金计算利息，不计算利息所产生的利息*/
float simpleInterest(int money,int year)
{
	//年利率为13%
	return money*(1+0.13*year);
}
/*复利计算：除了本金利息外，还要计算利息产生的利息*/
float compoundInterest(int money,int year)
{
	return money*pow((1+0.13),year);
}
