#include <stdio.h>

int factorial_0(int n);
double factorial_1(int n);

int main()
{
	int n;
	printf("请输入阶乘数(不超过12): ");
	scanf("%d",&n);
	printf("%d!=%d",n,factorial_0(n));
	return 0; 
 } 
 
int factorial_0(int n)
{
	return n<2? 1: n*factorial_0(n-1);
}
/*下面介绍100!阶乘的方法
**方法一: 用科学记数法表示,例:701000=7.01e+5=7.01x10^5.
**上面那种方法会损失精度,接下来介绍第二种方法;
**方法二: 对于超大位数储存,我们可以用数组存下. 
*/ 

double factorial_1(int  n)
{
	int i=0;
	double num=1.0;
	for(i=1;i<=n;++i)
	{
		num=num*i;
	}
	return num;
 } 
