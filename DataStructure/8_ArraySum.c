#include <stdio.h>

float arraySum(float [],int );

int main()
{
	int n=0,i=0;
	printf("Enter the number elements:");
	scanf("%d",&n);
	printf("Enter %d elements.\n",n);
	float arr[n];
	for(i=0;i<n;++i)
		scanf("%f",&arr[i]);
	printf("Sum=%.2f",arraySum(arr,n-1));
	return 0;
}

float arraySum(float arr[],int n)
{
	return n?arr[n]+arraySum(arr,n-1):arr[n];
}
