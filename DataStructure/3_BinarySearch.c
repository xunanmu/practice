#include <stdio.h>
#define LEN(arr)  sizeof(arr)/sizeof(arr[0])
#define MID ((low+high)/2) 

int binary_search_1(int [],int ,int ,int );
int binary_search_2(int [],int ,int ,int );

int main()
{
	int arr[]={1,3,8,11,21,30,45,69,78};
	int n;
	puts("1,3,8,11,21,30,45,69,78");
	printf("请输入你要查的数:");
	scanf("%d",&n);
	printf("%d的位置是第%d.",n,binary_search_1(arr ,n ,0 ,LEN(arr)-1)+1);
	return 0; 
}

int binary_search_1(int arr[],int n,int low,int high)
{
	//int mid=(low+high)/2;
	if(arr[MID]==n) 
		return MID;
	if(arr[MID]<n)
		low=MID+1;
	else
		high=MID-1;
	binary_search_1(arr,n,low,high);
}

int binary_search_2(int arr[],int n,int low,int high)
{
	//int mid=(low+high)/2;
	return n==arr[MID]? MID: n>arr[MID]? binary_search_2(arr,n,MID+1,high):binary_search_2(arr,n,low,MID-1);
}
