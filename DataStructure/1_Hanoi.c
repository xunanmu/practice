#include <stdio.h>

int hanoi(int ,char ,char ,char );

int main()
{
	int n;
	printf("请输入塔的层数:");
	scanf("%d",&n);
	/*a,b,c三柱塔针*/
	printf("移动次数:%d",hanoi(n,'a','b','c'));
	return 0;
 } 
 
 int hanoi(int n,char a,char b,char c)
 {
 	static count=0;
 	if(n==1)
 	{
 		++count;
 		printf("%c --->%c\n",a,c);
	}
	else
	{
		hanoi(n-1,a,c,b);
		++count;
		printf("%c --->%c\n",a,c);
		hanoi(n-1,b,a,c);
	}
	return count;
 }
