#include <stdio.h>

int natuaralSum_1(int n);//递归法 
int natuaralSum_2(int n);//公式法 

int main()
{
	int n=0;
	printf("请输入一个自然数:");
	scanf("%d",&n);
	printf("0到%d的和是:%d",n,natuaralSum_2(n) );
	return 0; 
}

int natuaralSum_1(int n)
{
	return n?n+natuaralSum_1(n-1):n;
}

int natuaralSum_2(int n)
{
	return (1+n)*n/2;
}
