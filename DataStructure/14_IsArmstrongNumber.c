/* 
	Author: 徐南木 
	Date: 11/09/19 20:13
	Description: 编写一个程序来读取数字，直到输入-1结束并显示它是水仙花数。
*/
#include <stdio.h>
#include <math.h>
int isArmstrongNumber(int );

int main()
{
	int n;
	printf("请输入数字(空格隔开,输入-1结束):\n");
	while(scanf("%d",&n))
	{
		if(n==-1)
			return 0;
		printf("%d%s水仙花数.\n",n,isArmstrongNumber(n)?"是":"不是"); 
	} 
}
int isArmstrongNumber(int n)
{
	int sum=0,t=n;
	do{
		sum+=pow(t%10,3);
	}while(t/=10);
	return sum==n;	
} 
