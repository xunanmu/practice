#include <stdio.h>
#define SWAP(x,y) {x=x+y;y=x-y;x=x-y;}

int main()
{
	int    a=101,b=320;
	float  c=1.3,d=2.3;
	double e=1.6,f=6.6;
	char   g='A',h='B';
	SWAP(a,b);
	SWAP(c,d);
	SWAP(e,f);
	SWAP(g,h);
	printf("%d,%d\n",a,b);
	printf("%.2f,%.2f\n",c,d);
	printf("%.2lf,%.2lf\n",e,f);
	printf("%c,%c\n",g,h);
	return 0;
}
