#include <stdio.h>
#define SWAP(x,y) (x^=y,y^=x,x^=y)

void selectionSort(int [],int );
void waySelectionSort(int [],int ); 

int main()
{
	int n=0,i=0;
	printf("Enter the number of items:");
	scanf("%d",&n);
	puts("Enter the items of sort.");
	int arr[n];
	for(i=0;i<n;++i)
		scanf("%d",&arr[i]);
	waySelectionSort(arr,n);
	puts("The sorted items are.");
	for(i=0;i<n;i++)
		printf("%d ",arr[i]);
	return 0;
}

void selectionSort(int arr[],int len)
{
	int i=0,j=0,pos=0;
	for(i=0;i<len-1;++i)
	{
		pos=i;
		for(j=i+1;j<len;++j)
			if(arr[pos]>arr[j]) pos=j;
		if(i!=pos)
			SWAP(arr[pos],arr[i]);
	}
}
/*双向选择排序 高低同时选择*/ 
void waySelectionSort(int arr[],int len)
{
	int low=0,high=len-1,pos=0,l,r; 
	while(low<high)
	{
		l=low;
		r=high;
		for(pos=low;pos<=high;++pos)
		{
			if(arr[pos]<arr[l])
				l=pos;
			if(arr[pos]>arr[r])
				r=pos;
		}
		if(l!=low)
			SWAP(arr[low],arr[l]);
		if(r!=high)
			r==low?SWAP(arr[high],arr[l]):SWAP(arr[high],arr[r]);
		--high;
		++low;
	}
}
