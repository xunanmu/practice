#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int g_eof;
int get_level(char c)
{
	if(c=='+'||c=='-')
		return 0;
	if(c=='^')
		return 2;
	return 1;
}

double get_val(double v[],char op)
{
	if(op=='+')
		return v[0]+v[1];
	if(op=='-')
		return v[0]-v[1];
	if(op=='*')
		return v[0]*v[1];
	if(op=='/')
		return v[0]/v[1];
	if(op=='^')
		return pow(v[0],v[1]);
	if(op=='%')
		return v[0]-(int)(v[0]/v[1])*v[1];
	return v[0];
}

double calc(char*op,int level,double val,char o)
{
	int i = (level>0);
	char c[2] = {
		o
	};
	double v[2] = {
		val
	};
	while(g_eof!=EOF)
	{
		if((g_eof = scanf("%lf%c",&v[i],&c[i]))==0
				&&scanf("%c",&c[i])==1)
		{
			if(c[i]=='(')
			{
				v[i] = calc(&c[i],0,0,0);
				scanf("%c",&c[i]);
				if(c[i]=='\n')
				{
					*op = '\n';
					return get_val(v,c[0]);
				}
			}
		}
		if(i>0)
		{
			if(!(c[i]=='\n'||c[i]==')'
						||get_level(c[0])>=get_level(c[1])))
			{
				v[i] = calc(&c[1],get_level(c[1]),v[1],c[1]);
			}
			v[0] = get_val(v,c[0]);
			c[0] = c[1];
		}
		else
			++i;
		if(c[0]=='\n'||c[0]==')'||get_level(c[0])<level)
		{
			*op = c[0];
			return v[0];
		}
	}
}

int main(void)
{
	char op;
	while(g_eof!=EOF)
	{
		printf("= %.15g\n",calc(&op,0,0,'\n'));
	}
	return 0;
}

