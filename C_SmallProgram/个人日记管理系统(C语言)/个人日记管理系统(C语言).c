# include<stdio.h>
# include<math.h>
# include<conio.h>
# include<string.h>
# include<windows.h>
# include<time.h>
/*
宏定义
ESC和Enter键的ascii值分别是27和13 
*/
#define Esc  27
#define Enter 13

/********************************日记结点类型*********************************/ 
typedef struct diary
{ 
  char date[15];     //日记日期 
  char title[20];    //日记标题 
  int  flag;         //用来标记该日记是否满足查阅要求，满足为-1，初始值为0；  
  struct diary *next;
}DiaryNode;            

/********************************用户结点类型 ********************************/ 
typedef struct user
{ 
  char user_name[20];   //用户名 
  char password[20];    //密码 
  int count;            //存储相应用户的日期篇数 
  DiaryNode *firstedge;
  struct user *next;
} *userList,userNode; 



/************************************函数声明**************************************/ 
void Show_Interface();
void Version_file();
userList  Init_user();
void main_meno(userList L); 
void User_Login(userList L);
userList User_apply(userList L);
void Help_file();
void User_Operate(userNode *H,userList L);
void Write_dairy(userNode *H,userList L);
int Search_dairy(userNode *H,char temp[]);
 void Read_dairy(char dairy_name[]);
void Delete_dairy(userNode *H,userList L);
void Rejigger_user_password(userNode *H,userList L);
void Consult_dairy_meno(userNode *H); 
void Consult_dairy(userNode *H,int i); 
void Express_chage_dark(char temp[]);
void Dark_chage_express(char temp[]);
void Password_input(char temp[]);
void Save_user(userList L);
void  Save_dairy(userNode *H);//保存文件 
/********************************************************************************/ 



/*欢迎界面*/
void Show_Interface()
{
	char xx[10][80];
	char res[80];
	int t=10;
	int  i,m,a,b,c;
	strcpy(xx[0],"\t\t\t个人日记管理系统\n");
	strcpy(xx[1],"\t\t\t\t算法设计:soulenvy\n");
	strcpy(xx[2],"\t\t\t\t代码编辑器:soulenvy\n");
	strcpy(xx[3],"\t\t\t\tFrame Design:Palo\n");
	strcpy(xx[4],"\t\t\t\tTest:soulenvy&Palo\n");
	strcpy(xx[5],"\t\t\t\tVersion 1.0\n");
	strcpy(xx[6],"\t\t\t\tPublish:B.S.P\n");
	strcpy(xx[7],"\t\t\t\tJune 8th 2019\n");

	do
	{
		for(i=0;i<80;i++) printf("*");
		printf("%s",xx[0]);
		for(i=0;i<80;i++) printf("*");
		printf("\n\n\n\n");
		printf("\t\t\t********************************\n");
		a=8-t; b=9-t;c=10-t;
		if(a>=1&&a<=7) strcpy(res,xx[a]);
		else strcpy(res,"\n");
		printf("%s",res);	
		if(b>=1&&b<=7) strcpy(res,xx[b]);
		else strcpy(res,"\n");
		printf("%s",res);
		if(c>=1&&c<=7) strcpy(res,xx[c]);
		else strcpy(res,"\n");
		printf("%s",res);	
		printf("\t\t\t********************************\n");
		printf("\n\n\n\t\t\t\t\t****\n");
		printf("\t\t\t\t\t*%02d*\n",t);
		printf("\t\t\t\t\t****\n");
		Sleep(1000);
		system("cls");
		t--;	
	}while(t>0);
}

void Version_file()  
{   
  FILE *fp;
  char c; 
  system("cls"); 
 Sleep(500);
  fp=fopen("version.txt","r");               //以只读的方式打开文件，将文件指针赋给fp 
  if(fp==NULL)   
   { 
      printf   ("\n-?- THE FILE CAN NOT BE OPENED......");   
      return ;  
   }   
  while(feof(fp)==0)
  {
  	c=fgetc(fp);                       //将文件中的字符读入
    printf("%c",c);
    Sleep(25);
  }	   
  fclose(fp);
  printf("\n\n\t"); 
  system("pause");  
  
} 
/******************************查阅日记操作*************************************/
void Consult_dairy(userNode *H,int i)
{
   DiaryNode *p;
   FILE *fp;
   char temp[20];     
   int j;
   int flag=0;
   p=H->firstedge;
   if (i==-1)        //进行顺序查询   
   {
      printf("\n\n-?- 请输入日记号码:\n--- ");
      scanf("%d",&j);
      if (j<=H->count)  
       { 
         while(j>1)
         {
           p=p->next;
           j--;
         }
        p->flag=-1;                //找到符合条件日记，对应结点的flag赋为-1，以便待会筛选出来  
        flag=1;
       }                                                                          
   }
   else
   { 
     if (i==0)   printf("\n-?- 请输入`时间:\n--- ");   //进行日期查询 
     if (i==15)   printf("\n-?- 请输入关键字:\n--- "); //进行关键字查询         
     scanf("%s",temp);
     rewind(stdin); 
     while(p!=NULL)
     {
        if(strstr((p->date+i),temp)!=NULL)   { p->flag=-1; flag=1;}    //找到符合条件的结点，其flag赋予-1，以便待会筛选出来   
        p=p->next; 
     } 
   }
   if(flag==0)  printf("\n\t\t####对不起日记找不到####");
    else
    {
      p=H->firstedge;
      printf("\n\t日记是:\n");
      chdir(H->user_name);
      while(p!=NULL)
      {
        if(p->flag==-1)
        {
          printf("\n\t—————————————————————————————————\n"); 
          printf("\t\tName:%s\t\tTime%s\n",p->title,p->date);               
          p->flag=0;                                        //筛选输出结点的flag为-1的日记名，日期 
          Read_dairy(p->title);
        }
        p=p->next;
      }
      chdir("..");
    }
    printf("\n\n\t\t");
    system("pause");

} 

/******************************查阅日记菜单*************************************/
void Consult_dairy_meno(userNode *H)
{
  char choice; 
  printf("\n\n\t   |^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^|\n");
  printf("\t   |1.By order\t2.By time\t3.By theme \t\t|\n");
  printf("\t   |^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^|\n");
  printf("\n-?- 选择一个项目(退出Esc)\n--- ");
  choice=getche();
  if (choice==Esc) return;
  switch(choice)
  {
    case'1':  Consult_dairy(H,-1);break;
    case'2':  Consult_dairy(H,0);break;
    case'3':  Consult_dairy(H,15);break;
  }  
}
    
/**************************************删除用户日记操作*********************************/
void Delete_dairy(userNode *H,userList L)
{
  DiaryNode *p;      //指向要删除的结点 
  DiaryNode *pre;    //指向要删除的结点的前驱结点 
  int i,j;
  char flag;
  printf("\n\n-?- 请输入删除的日记号码:\n--- ");
  scanf("%d",&i);
  rewind(stdin); 
  j=i; 
  p=H->firstedge;
  while(i>1)
  { 
    p=p->next;
    i--;
  }
  p->flag=-1;          //要删除的结点的flag标识符设为-1 
  p=H->firstedge;
  while(p!=NULL&&(p->flag==0))
  {
     pre=p;
     p=p->next;       
   } 
  printf("\n-$- 你想删除的日记是::\n");
  printf("\t\tNumber:%d\t   Name:%s\tTime:%s",j,p->title,p->date);
  printf("\n你确定删除这个日记吗:(Y or N) \t");
  scanf("%c",&flag);
  rewind(stdin); 
  if(flag=='Y'||flag=='y')   
  { 
    if(H->firstedge==p)   H->firstedge=p->next;      //要删除的结点为第一结点时 
       else   pre->next=p->next;                 //要删除的结点不是第一结点时 
   }  
   else 
   return ;
  chdir(H->user_name);              //把相应用户的目录弄为当前工作目录 
  H->count--;                       //用户的日记数减1 
  remove(p->title);                 //去掉相应的文件 
  free(p);                          //释放要删除的结点空间 
  Save_dairy(H);                    //保存用户日记信息，不然原有的用户日记信息还是不变  
  Save_user(L);                      //保存用户信息
  printf("\n\t删除中,请稍等.....");
  Sleep(1000);
  printf("\n\n\t日记已删除!!!\n\t");
  system("pause"); 
     
}

/********************************密文转换成明文**************************************/
void Dark_chage_express(char temp[])
{ 
  int i;
  i=0;
  while(temp[i]!='\0')
  {
    temp[i]=temp[i]+2;
    i=i+1;
  }
}

/********************************明文转换成密文*************************************/
void Express_chage_dark(char temp[])
{ 
  int i;
  i=0;
  while(temp[i]!='\0')
  {
    temp[i]=temp[i]-2;
    i=i+1;
  }
}

/********************************整数输入控制*************************************/
int Input_Integer()       
{  char str[10];
   long n,i=0;   
   scanf("%s",str);
   rewind(stdin);    
   while(str[i]!='\0')     //对字符串的各个字符进行判断，只是字符串结束
   {  
      if(str[i]<'0'||str[i]>'9')  return -1;
      i++;
    }
   n=atoi(str);          //利用atoi函数将字符串str转换成整型数
   return n;        //返回输入的正整数
}

/*****************初始化用户信息，读取系统已有的用户和用户相应的日记*******************/
userList  Init_user()
{
  FILE *fp1;
  FILE *fp2;
  userList L=NULL; 
  userNode *p; 
  DiaryNode *s;
  if ((fp1=fopen("Userlist","r+"))==NULL)   return L;
  while(!feof(fp1))
  {
   p=(userNode *)malloc(sizeof(userNode));
   if(fread(p,sizeof(userNode),1,fp1)!=1)  break; 
   p->firstedge=NULL;                                 //读取系统已有的用户信息 
   p->next=L;       
   L=p;                    
   chdir(p->user_name);                               //把相应用户目录作为当前工作目录 
   if((fp2=fopen("dairylist","r+"))==NULL) {chdir(".."); continue;}
     else
      while(!feof(fp2))
      {
        s=(DiaryNode *)malloc(sizeof(DiaryNode));             //读取系统相应用户的相应日记信息 
        if(fread(s,sizeof(DiaryNode),1,fp2)!=1)  break; 
        s->next=L->firstedge;       
        L->firstedge=s;
      }
   chdir("..");                                       //把父目录作为当前工作目录
   fclose(fp2);
   } 
   fclose(fp1);  
   return(L);     
}

/***********************************主菜单***************************************/
void main_meno(userList L) 
{
  char choice;	
	system("cls");

  do
  {
    printf("\n\t\t>>>>——Personal Diary System(v1.0)——<<<<\n");
    printf("\n\t\t|^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^|\n");
    printf("\t\t|\t1\tLogin\t      \t\t|\n\t\t|\t\t\t \t\t|\n\t\t|\t2\tRegister\t      \t|\n\t\t|\t\t \t\t\t|\n\t\t|\t3 \tHelp\t \t\t|\n");
    printf("\t\t|^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^|\n");
    printf("\n\n-?- Please choose an item(prees Esc to quit):\n---");
    choice=getche();
    if (choice==Esc) return; 
    switch(choice)
    {
      case '1' : User_Login(L);break;
      case '2' : L=User_apply(L);break;
      case '3' : Help_file();break; 
    }
   system("cls");
  }while(1); 
}
    
/***********************************打开帮助文件操作***************************************/
void Help_file()  
{   
  FILE *fp; 
  system("cls");  
  fp=fopen("help.txt","r");               //以只读的方式打开文件，将文件指针赋给fp 
  if(fp==NULL)   
   { 
      printf   ("\n-?- THE FILE CAN NOT BE OPENED......");   
      return ;  
   }   
  while(feof(fp)==0)                       //将文件中的字符读入
    { printf("%c",fgetc(fp)); Sleep(25); }  
  fclose(fp);
  system("pause");  
  
} 

/********************************密码输入控制函数***************************************/
void Password_input(char temp[])
{
  int i;
  i=0;
  while((temp[i]=getch())!=Enter) //输入回车键退出 
   { 
     if(temp[i]=='\b'&&i>0)   //当不是第一个输入删除键时 ，进行的操作 
       {
         rewind(stdin);  
         printf("\b");
         printf("  ");
         printf("\b\b");
         i--;
         continue; 
       }
        else
           if(temp[i]!='\b'&&i>=0) //当不是输入删除键时,输出*号
           {                                                                             
             printf("*");  
             i=i+1;      
           }       
   }   
  temp[i]='\0'; 
}

/***********************************读日记操作*****************************************/
 void Read_dairy(char dairy_name[])
 {
   FILE   *fp;
   char temp[200];
   int i=0;
   fp=fopen(dairy_name,"r"); 
   printf("\n\t日记的内容:\n\t\t");
   while(feof(fp)==0)  
     {
       fscanf(fp,"%c",&temp[i]);
       i++;
     }
   temp[i]='\0';
   Dark_chage_express(temp);
   printf("%s\n",temp); 
   fclose(fp);
}

/****************************************修改用户密码*操作********************************/
void Rejigger_user_password(userNode *H,userList L)
{
   char temp1[20];      //用于保存原有的密码 ,设计这个有利于密码的更有效率的保存 
   char temp2[20];      //用于保存第一次输入的密码 
   char temp3[20];      //用于保存第二次输入的密码 
   char flag;    
   strcpy(temp1,H->password);
   Dark_chage_express(temp1);
   printf("\n\n\tYour password is \t\t%s",temp1);
   printf("\n\n-?- Please input the password:\n--- ");
   Password_input(temp2);
   printf("\n-?- Please input the password again\n--- "); 
   Password_input(temp3);
   while(strcmp(temp2,temp3)!=0)           //两二次输入的密码一样          
   {
     printf("\n-!- THE PASSWORDS IS NOT THE SAME!!!\n");
     printf("\n\n-?- Please input the password:\n--- ");
     Password_input(temp2);
     printf("\n-?- Please input the password again\n--- "); 
     Password_input(temp3);                                                
    } 
   Express_chage_dark(temp2);
   strcpy(H->password,temp2);
   printf("\n\n\t\t密码修改中 ,please wait......");
   Sleep(1);  
   Save_user(L);                                 //系统休眠5秒 
   printf("\n\n\t\t####密码已更改!!!####\n\n\t");          
   system("pause");
}
       
/********************************保存相应用户日记信息*********************************/
void  Save_dairy(userNode *H)
 { 
   FILE *fp;  
   DiaryNode *p; 
   chdir(H->user_name);           
   if((fp=fopen("dairylist","w"))==NULL)       
    {
      printf("-!- CAN NOT OPEN THE FILE!!!\n");
      system("PAUSE");
       return;       
    }
   p=H->firstedge;
   while(p!=NULL)
   {
    fwrite(p,sizeof(DiaryNode),1,fp);
    p=p->next;         
   }
   chdir("..");
   fclose(fp);                      
}

/********************************保存全部用户信息*********************************/
void Save_user(userList L)
{
  FILE *fp;
  userNode *p;
  fp=fopen("Userlist","w");      
  p=L;
  while(p!=NULL)
  {
    fwrite(p,sizeof(userNode),1,fp);
    p=p->next;         
  }
  fclose(fp);          
}

/*********************************查找是否有同名日记操作************************************/
int Search_dairy(userNode *H,char temp[])
{
    DiaryNode *p;
    p=H->firstedge;
    while(p!=NULL&&strcmp(p->title,temp)!=0)
    p=p->next;
    if(p!=NULL) return 0;
      else return 1;            

}

/***********************************申请新用户操作*****************************************/
userList User_apply(userList L)
{
  char temp1[20];
  char temp2[20];
  int i;
  char flag;
  userNode *p;
  userNode *s; 
  rewind(stdin); 
  s=(userNode *)malloc(sizeof(userNode));
  system("cls");
  printf("\n\t\t\t|^^^^^^^^^^^^^^^^^^^^^^^|\n");
  printf("\t\t\t|\tRegister\t|\n");
  printf("\t\t\t|^^^^^^^^^^^^^^^^^^^^^^^|\n");
  printf("\n\tUser list:\n");
  p=L;
  while(p!=NULL)
  {
     printf("\t\t\t%s\n",p->user_name);
     p=p->next;                                 //输出系统已存在的用户名，以让用户申请新用户时，有个注意，不申请重复的用户名 
  } 
  p=L;
  printf("\n\n-?- 请输入一个用户名:\n--- ");
  scanf("%s",s->user_name);
  rewind(stdin); 
  while(p!=NULL&&strcmp(p->user_name,s->user_name)!=0)
    p=p->next;                                            //判断有没有重复的用户名 
  while(p!=NULL) 
  { 
   printf("\n-!- 名字已存在!!!!!\n");
   printf("\n-?- 是否继续?(Y or N)\n--- ");
   scanf("%c",&flag);       
   if (flag=='N'||flag=='n'){free(s); return L;}
   printf("\n-?- Please input the name:\n--- ");
   scanf("%s",s->user_name);
   rewind(stdin);                                   
   p=L;
   while(p!=NULL&&strcmp(p->user_name,s->user_name)!=0)
   p=p->next;
  } 
   printf("\n\t\t\t#####The name can be used.#####\n");
   printf("\n-?- Please input the password:\n--- "); 
   Password_input(temp1);
   printf("\n-?- Please input the password again:\n--- "); 
   Password_input(temp2);
   while(strcmp(temp1,temp2)!=0)
   {
     printf("\n-!- THE PASSWORDS IS NOT THE SAME!!!\n");
     printf("\n-?- Please input the password:\n--- "); 
     Password_input(temp1);
     printf("\n-?- Please input the password again:\n--- "); 
     Password_input(temp2);                                                
    } 
   strcpy(s->password,temp1); 
   s->firstedge=NULL;
   s->count=0;                           //申请好用户，进行相应的操作 
   mkdir(s->user_name);                  //生成相应用户对应的文件夹，文件夹里用来存储相应用户的相应日记  
   Express_chage_dark(s->password);      //进行密码家秘密 
   s->next=L;
   L=s;
   Save_user(L);                        //每添加一个用户时，都要进行存储用户信息的保存 
   printf("\n\t\tRegistering please wait..."); 
   Sleep(2000);
   printf("\n\n\t\t####祝贺你的申请成功####\n\n\t");
   system("pause");  
   return(L);
 }

/***********************************相应用户登录后的操作***************************************/
void User_Operate(userNode *H,userList L)
{
 char choice;
 DiaryNode *p; 
 int i;
  system("cls");
  do
  {
    printf("\t\t  >>>>———%s ,Welcome to the system———<<<<\n\n",H->user_name);
    printf("\t|^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^|\n");
    printf("\t|  1.新增       2.查看      3.删除    4.修改密码    \t|\n");
    printf("\t|^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^|\n");
    printf("\n\tDiary list:\n");
    p=H->firstedge;
    printf("\n\t\t|^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^|\n");
    printf("\t\t|\tNO.\t   theme\ttime\t        |\n");
    p=H->firstedge;
    i=1;
    while(p!=NULL)
   {
     printf("\t\t|\t%d\t  %s\t%s\t|\n",i,p->title,p->date);
     p=p->next;                                          //输出相应用户的日记名 
     i++; 
   }          
   printf("\t\t|^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^|\n"); 
   printf("\n\n-?- Please choose an item(Esc for qiut):\n--- ");
   choice=getche();
   if (choice==Esc) return; 
     switch(choice)
    {
      case '1': Write_dairy(H,L);break;                   //写日记                
      case '2': Consult_dairy_meno(H);break;              //查阅日记
      case '3': Delete_dairy(H,L);break;                    //删除日记
      case '4': Rejigger_user_password(H,L);break;          //修改用户 
    }
    system("cls");
   }while(1); 
}     

/***********************************用户登录操作***************************************/
void User_Login(userList L)
{ 
  char temp[20];
  char flag;
  int i,j,count;
  userNode *p;
  DiaryNode *s;
  p=L;
  system("cls");
  printf("\n\t\t\t|^^^^^^^^^^^^^^^^^^^^^^^|\n");
  printf("\t\t\t|\tLogin\t\t|\n");
  printf("\t\t\t|^^^^^^^^^^^^^^^^^^^^^^^|\n");
  printf("\n\tUser list:\n");
  printf("\t\t|^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^|\n");
  printf("\t\t|\tNO.\t    name\t\t|\n");
  count=1;
  while(p!=NULL)
  {
     printf("\t\t|\t%d\t    %s\t\t|\n",count,p->user_name);
     p=p->next;
     count++;
  }                                       //显示系统全部的用户信息,供用户选择登入 
  printf("\t\t|^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^|\n");
  count--;
  printf("\n\n-?- Choose a number:\n--- ");       
  i=Input_Integer();
  while(i>count||i==-1) 
  {
   printf("\n\n-!- IUPUT ERROR");
   printf("\n-?- continue to login?(Y or N)\n--- ");
   scanf("%c",&flag);
   if(flag=='N'||flag=='n') return;
   printf("\n\n-?- choose a number:\n--- ");
   scanf("%d",&i);
  } 
  j=1;
  p=L;
  while(j<i)
  {
    j++;
    p=p->next;
  }
  printf("\n-$- You has choosen:\t\t%s",p->user_name);
  printf("\n-?- Iuput the password:\n--- ");  
  Password_input(temp);              //输入密码 
  Express_chage_dark(temp);          //明码转化为暗码，以跟以保存的用户密码比较 
  while(strcmp(p->password,temp)!=0)
  {
    printf("\n-!- WRONG PASSWORD!!!\n");
    printf("\n-?- Continue to input?(Y or N)\n--- ");
    scanf("%c",&flag);
    if (flag=='N'||flag=='n') return; 
    printf("\n-?- Input the password:\n--- "); 
    Password_input(temp); 
    Express_chage_dark(temp);                                              
  }  
  printf("\n\n\t\t\tLogining please wait...");
  Sleep(2000);
  system("cls"); 
  User_Operate(p,L);              //用户登入后，进行用户操作 
}

/***********************************写日记操作*****************************************/
 void Write_dairy(userNode *H,userList L)
{
  char choice;    
  char temp[500];                    //用于存储写入的日记 
  FILE *fp;
  DiaryNode *s;
  int i;
  printf("\n\n-?- Please input the content of new diary:\n\n--- ");
  gets(temp);
  rewind(stdin); 
  Express_chage_dark(temp);                //对日记内容加密 
  s=(DiaryNode *)malloc(sizeof(DiaryNode));               
  printf("\n\n-?- Please iuput the name for this diary:\n--- "); 
  scanf("%s",s->title);
  rewind(stdin); 
  while(Search_dairy(H,s->title)!=1)
  {
    printf("\n-!- THE NAME HAS EXISTED!!!!");
    printf("\n\n-?- Please iuput the name for this diary:\n--- "); 
    scanf("%s",s->title); 
                
  }
  printf("\n\t\t####The name is OK.####");
  printf("\n-?- input the date like 2011\\05\\15\n--- ");
  scanf("%s",s->date);
  rewind(stdin); 
  s->flag=0;
  chdir(H->user_name);                   //把相应用户目录作为当前工作的目录 
  if((fp=fopen(s->title,"w"))==NULL)
    {
      printf("-!- SORRY,THE FILES CAN NOT BE OPENED!!!\n");
      return;
      }        
  i=0;
  printf("\n\tSaving please waiting...");
  while(temp[i]!='\0') 
  {
    fwrite(temp+i,1,1,fp);
    i=i+1;
  }
  Sleep(1);                        //存储日记，sleep(5000)使窗体等待一段时间，以使用户感觉程序正在保存文件 
  s->next=H->firstedge;
  H->firstedge=s;
  H->count++;
  chdir("..");                          //转移到父目录作为当前工作的目录        
  fclose(fp);                          
  Save_dairy(H);                       //保存相应用户的日记信息   
  Save_user(L);                        //保存用户信息，因为此时用户的count已经改变了，需要保存 
  printf("\n\n-$- The diary has been saved\n\n\t");
  system("pause"); 
}

/*************************************主函数*****************************************/
int main()
{
  userList L;
  Show_Interface();
  Version_file();
  L=Init_user();
  main_meno(L);
  return 0;
}
